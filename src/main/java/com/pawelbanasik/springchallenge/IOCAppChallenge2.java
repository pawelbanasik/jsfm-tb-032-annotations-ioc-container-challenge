package com.pawelbanasik.springchallenge;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class IOCAppChallenge2 {

	public static void main(String[] args) {


		ApplicationContext context = new AnnotationConfigApplicationContext(ConfigProperty.class);
		
		City city = (City) context.getBean("cityBeanId");

		city.cityName();

		((AbstractApplicationContext) context).close();
	}
}
