package com.pawelbanasik.springchallenge;

import org.springframework.stereotype.Component;

@Component("cityBeanId")
public class City {

	public void cityName() {
		
		System.out.println("City name is Adelaide.");
	}
	
}
